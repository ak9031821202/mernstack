const fs = require("fs");
const model = require("../model/product");
const Product = model.Product;

exports.createProduct = async (req, res) => {
  console.log("req", req.body);
  const product = new Product(req.body);

  try {
    const doc = await product.save();
    console.log({ doc });
    res.status(201).json({ doc });
  } catch (err) {
    res.status(404).json({ err });
    console.log({ err });
  }
};

exports.getAllProducts = async (req, res) => {
  const product = await Product.find({});
  res.json(product);
};

exports.getProduct = async (req, res) => {
  const id = req.params.id;
  const product = await Product.findById(id);
  res.json(product);
};
exports.replaceProduct = async (req, res) => {
  const id = req.params.id;
  try {
    const doc = await Product.findOneAndReplace({ _id: id }, req.body, {
      new: true,
    });
    res.status(201).json(doc);
  } catch {
    (error) => {
      res.status(404).json(error);
    };
  }
};
exports.updateProduct = async (req, res) => {
  const id = req.params.id;
  try {
    const doc = await Product.findOneAndUpdate({ _id: id }, req.body, {
      new: true,
    });
    res.status(201).json(doc);
  } catch {
    (error) => {
      res.status(404).json(error);
    };
  }
};
exports.deleteProduct = async (req, res) => {
  const id = req.params.id;
  console.log("id", id);
  try {
    const doc = await Product.findOneAndDelete({ _id: id });
    res.status(201).json(doc);
  } catch {
    (error) => {
      res.status(404).json(error);
    };
  }
};

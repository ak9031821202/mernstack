//File System ES6 Js Module////////////////////////////////////////
//  import { sum, diff } from "./lib.js";
// console.log(sum(6, 8), diff(9, 7));

// File System Common Js Module////////////////////////////////////
// const lib = require("./lib.js");
// const fs = require("fs");
// const t1 = performance.now();
// const txt = fs.readFileSync("./demo.txt", "utf-8");
// fs.readFile("./demo.txt", "utf-8", (err, txt) => {
//   console.log(txt);
// });
// console.log(txt);
// console.log(lib.sum(6, 8), lib.diff(9, 7));
// const t2 = performance.now();
// console.log(t2 - t1);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////

// console.log("Hello word");

// const fs = require("fs");
// const http = require("http");

// const posts = index.posts;

// const server = http.createServer((req, res) => {
//   switch (req.url) {
//     case "/login":
//       res.writeHead(200, { "Content-Type": "application/json" });
//       res.write(index);
//       res.end();
//       break;
//     case "/home":
//       res.writeHead(200, { "Content-Type": "text/html" });
//       res.write("<h1>Hello World</h1>");
//       res.end();
//       break;
//     default:
//       res.writeHead(200, { "Content-Type": "text/html" });
//       res.write("<h1>Bye</h1>");
//       res.end();
//   }
// });

// server.listen(8080);

// ///////////////////////////////////////////////////////Express Js //////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// //////////////////// REST API /////////////////////////////////////////////////////////////////////////////

// Create ==> POST ==> Data in Request Body ====>>  /students

// Read ==> GET ==> Find Data in URL Params ====>>  /students/:id
// GET ===> NO Data                ====>> /students

// Create ==> PUT ==> Find Data in URL Params ====>>  /students/:id
// PUT ==> Update Data in Rrquest body

// Delete ==> DELETE ==> Find Data in URL Params ===> /studennts/:id

// const express = require("express");

// const server = express();

// server.use(express.json());

// //CREATE POST API '/posts'
// server.post("/posts", function (req, res) {
//   posts.push(req.body);
//   res.json(req.body);
// });

// //READ GET API '/post'
// server.get("/posts", function (req, res) {
//   res.json(posts);
// });

// //READ GET API '/post/:id'
// server.get("/posts/:id", function (req, res) {
//   const id = +req.params.id;
//   const post = posts.find((p) => p.id === id);
//   console.log(id);
//   res.json(post);
// });

// //UPDATE PUT API '/post/:id'
// server.put("/posts/:id", function (req, res) {
//   const id = +req.params.id;
//   const postIndex = posts.findIndex((p) => p.id === id);
//   posts.splice(postIndex, 1, { ...req.body, id: id });
//   console.log(id);
//   res.status(201).json();
// });

// //UPDATE PATCH API '/post/:id'
// server.patch("/posts/:id", function (req, res) {
//   const id = +req.params.id;
//   const postIndex = posts.findIndex((p) => p.id === id);
//   const post = posts[postIndex];
//   posts.splice(postIndex, 1, { ...post, ...req.body });
//   console.log(id);
//   res.status(201).json();
// });

// //DELETE DELETE API '/post/:id'
// server.delete("/posts/:id", function (req, res) {
//   const id = +req.params.id;
//   const postIndex = posts.findIndex((p) => p.id === id);
//   const post = posts[postIndex];
//   posts.splice(postIndex, 1);
//   console.log(id);
//   res.status(201).json(post);
// });

require("dotenv").config();

const express = require("express");
const server = express();
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
const productRouter = require("./routes/product");
const userRouter = require("./routes/user");

console.log(process.env.DB_PASSWORD_OR, process.env.DB_USERNAME);

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
    });
    console.log(`MongoDB Connected: {conn.connection.host}`);
  } catch (error) {
    console.error(error.message);
    process.exit(1);
  }
};

connectDB();

//bodyParser
server.use(cors());
server.use(express.json());
server.use(express.static(path.resolve(__dirname, process.env.PUBLIC_DIR)));
server.use("/products", productRouter.router);
server.use("/users", userRouter.router);
server.use("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "build", "index.html"));
});

server.listen(8080, () => {
  console.log("Server Started");
});

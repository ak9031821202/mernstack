const mongoose = require("mongoose");
const { Schema } = mongoose;

const productSchema = new Schema({
  title: { type: String, require: true },
  description: String,
  price: {
    type: Number,
    min: [0, "wrong price"],
    require: true,
  },
  discountPercentage: {
    type: Number,
    min: [0, "min discount wrong"],
    max: [50, "max discount wrong"],
  },
  rating: {
    type: Number,
    min: [0, "min rating wrong"],
    max: [5, "max rating wrong"],
  },
  stock: Number,
  brand: String,
  category: { type: String, require: true },
  thumbnail: { type: String, require: true },
  images: [String],
});

exports.Product = mongoose.model("Product", productSchema);
